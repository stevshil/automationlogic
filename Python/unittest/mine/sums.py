#!/usr/bin/env python3

class sums:

    def __init__(self):
        self.result=0.0

    def add(self, a,b):
        self.result=a+b
        return self.result

    def subtract(self,a,b):
        self.result=a-b
        return self.result

    def multiply(self,a,b):
        self.result=a*b
        return self.result

if __name__ == "__main__":
    x=sums()
    print(x.add(1,2))
