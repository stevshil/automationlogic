create database if not exists msOrder;

use msOrder;

DROP TABLE if exists SaleLine;
DROP TABLE if exists SaleOrd;
DROP TABLE if exists Product;
DROP TABLE if exists Supplier;

CREATE TABLE Product (
 PrdID		CHAR(3)	NOT NULL,
 Descr		VARCHAR(30)	NOT NULL,
 Cost		DECIMAL(6,2)	NOT NULL,
 Sell		DECIMAL(6,2)	NOT NULL,
 Instock	INTEGER	DEFAULT 0,
 Primary Key (PrdID)
);

CREATE TABLE Supplier (
 SupID		CHAR(4)	NOT NULL,
 Name		VARCHAR(30)	NOT NULL,
 Addr1		VARCHAR(30),
 Addr2		VARCHAR(30),
 Town		VARCHAR(30),
 County		VARCHAR(30),
 Postcode	VARCHAR(8)	NOT NULL,
 Credit		CHAR(1)	DEFAULT 'A',
 Primary Key (SupID)
);

CREATE TABLE SaleOrd (
 OrdID		INTEGER	NOT NULL,
 SupID		CHAR(4)	NOT NULL,
 DateOrd	DATETIME	NOT NULL,
 DateDue	DATETIME,
 Comp		CHAR(1),
 Primary Key (OrdID)
);

CREATE TABLE SaleLine (
 OrdID		INTEGER	NOT NULL,
 PrdID		CHAR(3)	NOT NULL,
 Qty		DECIMAL	NOT NULL,
 Recd		CHAR(1),
 PRIMARY KEY (OrdID, PrdID),
 FOREIGN KEY (PrdID) REFERENCES Product(PrdID),
 CONSTRAINT chkrecd CHECK (Recd='Y' OR Recd='N' )
);

--
--  INSERT VALUES INTO PRODUCT TABLE
--  --------------------------------

INSERT INTO Product VALUES( 'BK1','A Guide to SQL',14.5,18.56,20);
INSERT INTO Product VALUES( 'BK2','Access Database Design',20.6,25.75,30);
INSERT INTO Product VALUES( 'CA1','Kodak DC220 Zoom Camera',391.1,486.92,5);
INSERT INTO Product VALUES( 'DR1','Iomega Zip Drive - 100 MB',93,118.11,10);
INSERT INTO Product VALUES( 'DR2','Iomega Zip Disc x 3',20,24.55,11);
INSERT INTO Product VALUES( 'FU1','Canon Multipass C50',540.25,634.79,1);
INSERT INTO Product VALUES( 'FU2','Hewlett Packard Officejet 700',367.5,459.38,2);
INSERT INTO Product VALUES( 'KB1','Canon Keyboard',23.5,29.61,25);
INSERT INTO Product VALUES( 'KB2','Microsoft Keyboard',47.5,60.8,37);
INSERT INTO Product VALUES( 'KB3','Logitec Cordless Keyboard',50,64,50);
INSERT INTO Product VALUES( 'LT1','Hewlett Packard Jornada 820e',663,822.12,1);
INSERT INTO Product VALUES( 'LT2','LG Phenom 100% Express',469,581.56,10);
INSERT INTO Product VALUES( 'LT3','IBM Thinkpad I',1093,1289.74,2);
INSERT INTO Product VALUES( 'LT4','Fujitsu Lifebook C325',820,1049.6,3);
INSERT INTO Product VALUES( 'MM1','Mouse Mat - Wallace and Gromit',8.2,10.5,60);
INSERT INTO Product VALUES( 'MM2','Mouse Mat - Plain',4.75,6.08,90);
INSERT INTO Product VALUES( 'MO1','Taxan Ergovision 735 17"',250,320,8);
INSERT INTO Product VALUES( 'MO2','Taxan Ergovision 550 15"',200,256,12);
INSERT INTO Product VALUES( 'MO3','CTX 17" Monitor',240,300,14);
INSERT INTO Product VALUES( 'MU1','Logitec Cordless Mouse',45.75,56.96,50);
INSERT INTO Product VALUES( 'MU2','Logitec Wheel Mouse',20,25.4,55);
INSERT INTO Product VALUES( 'MU3','Canon Cruise Internet Touchpad',66,81.02,18);
INSERT INTO Product VALUES( 'PR1','Canon BJC-4200 Printer',150,176.25,9);
INSERT INTO Product VALUES( 'PR2','Epson Stylus Color 740',174.5,218.13,12);
INSERT INTO Product VALUES( 'PR3','Epson 300 Color Inkjet',125,157.5,12);
INSERT INTO Product VALUES( 'SC1','Canon Genius Scanner',99.99,127.99,20);
INSERT INTO Product VALUES( 'SC2','Hewlett Packard Scanjet 4100C',137,175.36,6);
INSERT INTO Product VALUES( 'SC3','Umax Astra 122OU Scanner',100,128,12);
INSERT INTO Product VALUES( 'SC4','Canon Snapscan 1212U',128,163.84,10);
INSERT INTO Product VALUES( 'TW1','Patriot 200 MHz Tower',450.5,563.13,19);
INSERT INTO Product VALUES( 'TW2','Olivetti 166 MHz Tower',375,480,10);
INSERT INTO Product VALUES( 'TW3','Mesh Connect Ultra 350MHz',500,610,3);

--
--  INSERT VALUES INTO SUPPLIER TABLE
--  ---------------------------------

INSERT INTO Supplier VALUES( 'COMT','Comet Ltd','Unit 2','Clifton Moor','York',NULL,'Y010 2AB','A');
INSERT INTO Supplier VALUES( 'DELL','Dell Computers','Millbank House','Western Road','Bracknell', 'Berks','RG12 1HD','B');
INSERT INTO Supplier VALUES( 'DIXN','Dixons Ltd','36 Coney_Street',NULL,'York',NULL,'Y030 1GZ','A');
INSERT INTO Supplier VALUES( 'PCWD','P C World','Monks Cross',NULL,'York',NULL,'Y020 1DC','A');
INSERT INTO Supplier VALUES( 'TINY','Tiny Computers','3 Colliergate',NULL,'York',NULL,'Y020 3EX','B');
INSERT INTO Supplier VALUES( 'CMAN','Computer Manuals',NULL,NULL,'Birmingham',NULL,'B27 6BR','C');
INSERT INTO Supplier VALUES( 'GATE','Gateway Computers','10 Bedford Street','Covent Garden','London', NULL,'W13 1CV','C'); 
INSERT INTO Supplier VALUES( 'EVES','Evesham Vale','Gloucester Court','Gloucester Terrace','Leeds', 'West Yorkshire','LS12 4GB','A');

--
--  INSERT VALUES INTO SALEORD TABLE
--  --------------------------------

INSERT INTO SaleOrd VALUES( 100,'COMT','1999-01-12','1999-02-11',NULL);
INSERT INTO SaleOrd VALUES( 101,'DIXN','1999-01-24','1999-02-21',NULL);
INSERT INTO SaleOrd VALUES( 102,'DIXN','1999-04-03','1999-04-15',NULL);
INSERT INTO SaleOrd VALUES( 103,'CMAN','1999-03-02',NULL,NULL);
INSERT INTO SaleOrd VALUES( 104,'EVES','1999-04-23',NULL,NULL);
INSERT INTO SaleOrd VALUES( 105,'TINY','1999-01-08',NULL,NULL);
INSERT INTO SaleOrd VALUES( 106,'DELL','1999-04-12','1999-05-07',NULL);
INSERT INTO SaleOrd VALUES( 107,'CMAN','1999-04-17','1999-05-18',NULL);
INSERT INTO SaleOrd VALUES( 108,'PCWD','1999-02-02',NULL,NULL);
INSERT INTO SaleOrd VALUES( 109,'COMP','1999-01-03',NULL,NULL);
INSERT INTO SaleOrd VALUES( 110,'CMAN','1999-02-03',NULL,NULL);

--
--  INSERT VALUES INTO SALELINE TABLE
--  ---------------------------------

INSERT INTO SaleLine VALUES( 100,'KB3',55,'Y');
INSERT INTO SaleLine VALUES( 100,'SC2',35,'Y');
INSERT INTO SaleLine VALUES( 101,'DR1',1,NULL);
INSERT INTO SaleLine VALUES( 101,'DR2',2,NULL);
INSERT INTO SaleLine VALUES( 101,'KB3',1,NULL);
INSERT INTO SaleLine VALUES( 101,'MO3',1,NULL);
INSERT INTO SaleLine VALUES( 101,'MU1',1,NULL);
INSERT INTO SaleLine VALUES( 101,'PR2',1,NULL);
INSERT INTO SaleLine VALUES( 101,'SC2',1,NULL);
INSERT INTO SaleLine VALUES( 101,'TW1',1,NULL);
INSERT INTO SaleLine VALUES( 103,'TW1',20,'Y');
INSERT INTO SaleLine VALUES( 104,'FU1',5,'N');
INSERT INTO SaleLine VALUES( 104,'LT1',6,'N');
INSERT INTO SaleLine VALUES( 104,'SC2',25,'N');
INSERT INTO SaleLine VALUES( 105,'KB3',6,'N');
INSERT INTO SaleLine VALUES( 105,'TW1',12,'N');
INSERT INTO SaleLine VALUES( 105,'MU3',2,'N');
INSERT INTO SaleLine VALUES( 105,'DR1',20,'N');
INSERT INTO SaleLine VALUES( 105,'PR3',25,'N');
INSERT INTO SaleLine VALUES( 107,'BK1',10,NULL);
INSERT INTO SaleLine VALUES( 107,'BK2',15,NULL);
INSERT INTO SaleLine VALUES( 108,'MO1',100,'Y');
INSERT INTO SaleLine VALUES( 108,'MO3',250,'Y');

