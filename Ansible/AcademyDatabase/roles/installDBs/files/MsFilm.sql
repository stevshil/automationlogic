CREATE DATABASE if not exists msFilm;

use msFilm;

DROP TABLE if exists Rental;
DROP TABLE if exists Film;
DROP TABLE if exists Category;
DROP TABLE if exists Member;
DROP TABLE if exists Director;

CREATE TABLE Category (
 CatID	INTEGER	NOT NULL,
 Descr	VARCHAR(30)	NOT NULL,
 Primary Key (CatID)
);

CREATE TABLE Director (
 DirID	INTEGER	NOT NULL,
 `Name`		VARCHAR(30)	NOT NULL,
 Born		INTEGER,
 Died		DECIMAL(4),
 Primary Key (DirID)
);

CREATE TABLE Member (
 MemID	INTEGER	NOT NULL,
 Name		VARCHAR(30)	NOT NULL,
 Addr1	VARCHAR(30)	NOT NULL,
 Addr2	VARCHAR(30),
 Addr3	VARCHAR(30),
 PostCode	CHAR(8),
 Payment	CHAR(8),
 Primary Key (MemID)
);

CREATE TABLE Film (
 FilmID	INTEGER	NOT NULL,
 Title	VARCHAR(30)	NOT NULL,
 DateMade	INTEGER	NOT NULL,
 Cert		CHAR(2),
 Cost		DECIMAL(4,2),
 CatID	INTEGER,
 DirID	INTEGER,
 Primary Key (FilmID),
 FOREIGN KEY (DirID) REFERENCES Director (DirID),
 FOREIGN KEY (CatID) REFERENCES Category (CatID) 
);

CREATE TABLE Rental (
 MemID		INTEGER	NOT NULL,
 FilmID		INTEGER	NOT NULL,
 DateRented		DATETIME		NOT NULL,
 DateReturned	DATETIME,
 FOREIGN KEY (MemID) REFERENCES Member(MemID),
 FOREIGN KEY (FilmID) REFERENCES Film(FilmID),
 PRIMARY KEY (MemID, FilmID)
);

--
--  INSERT VALUES INTO CATEGORY TABLE
--  ---------------------------------

INSERT INTO Category VALUES( 1,'Comedy');
INSERT INTO Category VALUES( 2,'Drama');
INSERT INTO Category VALUES( 3,'Western');
INSERT INTO Category VALUES( 4,'Science Fiction');
INSERT INTO Category VALUES( 5,'Horror');

--
--  INSERT VALUES INTO Director TABLE
--  ---------------------------------

INSERT INTO Director VALUES( 1,'Woody Allen',1935,NULL);
INSERT INTO Director VALUES( 2,'Alfred Hitchcock',1899,1980);
INSERT INTO Director VALUES( 3,'Cecile B De Mille',1881,1959);
INSERT INTO Director VALUES( 4,'Stanley Kramer',1913,NULL);
INSERT INTO Director VALUES( 5,'Stanley Kubrick',1928,NULL);
INSERT INTO Director VALUES( 6,'Otto Preminger',1906,NULL);
INSERT INTO Director VALUES( 7,'John Ford',1895,1973);

--
--  INSERT VALUES INTO Member TABLE
--  -------------------------------

INSERT INTO Member VALUES( 1001,'Alan Evans','25 Pasture Close','Campden','London','LS1 3DG','Cash');
INSERT INTO Member VALUES( 1002,'Jennie Jones','14 St Johns Wood Road',NULL,'Glasgow','GW6 1DR','Visa');
INSERT INTO Member VALUES( 1003,'John Smith','Bleak House','The Crescent','Bath','BA5 8LT','Access');
INSERT INTO Member VALUES( 1004,'Alex Adam','101 Holly Street','Hampstead','London','LS2 4GH','Cash');
INSERT INTO Member VALUES( 1005,'Chloe Smedley','14 Fairbrother Road','West Mill','Bristol','BR2 1LS','Cash');

--
--  INSERT VALUES INTO FILM TABLE
--  ---------------------------------

INSERT INTO Film VALUES( 100,'Anatomy of a Murder',1959,'NR',2,2,6);
INSERT INTO Film VALUES( 101,'Clockwork Orange',1971,'R',2.5,4,5);
INSERT INTO Film VALUES( 102,'Dr Strangelove',1964,'PG',2,1,5);
INSERT INTO Film VALUES( 103,'Grapes of Wrath',1940,'NR',1.75,2,7);
INSERT INTO Film VALUES( 104,'Interiors of the Earth',1978,'PG',1.5,2,1);
INSERT INTO Film VALUES( 105,'Laura',1944,'NR',2,2,6);
INSERT INTO Film VALUES( 106,'Mogambo',1953,'NR',1.75,3,7);
INSERT INTO Film VALUES( 107,'North by Northwest',1959,'PG',1.75,2,2);
INSERT INTO Film VALUES( 108,'Psycho',1960,'PG',2.5,5,2);
INSERT INTO Film VALUES( 109,'Rope',1948,'NR',2,2,2);
INSERT INTO Film VALUES( 110,'Stagecoach',1939,'NR',1.75,3,7);
INSERT INTO Film VALUES( 111,'The Birds',1963,'NR',2,5,2);
INSERT INTO Film VALUES( 112,'The Man with the Golden Arm',1955,'NR',1.75,2,6);
INSERT INTO Film VALUES( 113,'The Moon is Blue',1953,'NR',1.75,1,6);

--
--  INSERT VALUES INTO RENTAL TABLE
--  -------------------------------
INSERT INTO Rental VALUES( 1001,101,'1999-01-02','1999-01-04');
INSERT INTO Rental VALUES( 1002,103,'1999-01-10','1999-01-11');
INSERT INTO Rental VALUES( 1003,100,'1999-02-04','1999-02-04');
INSERT INTO Rental VALUES( 1003,111,'1999-03-06',NULL);
INSERT INTO Rental VALUES( 1003,107,'1999-01-14','1999-01-16');
INSERT INTO Rental VALUES( 1004,101,'1999-02-11',NULL);
INSERT INTO Rental VALUES( 1004,108,'1998-12-14','1998-12-19');
INSERT INTO Rental VALUES( 1005,111,'1999-02-05',NULL);
INSERT INTO Rental VALUES( 1005,108,'1999-02-12','1999-02-13');
INSERT INTO Rental VALUES( 1005,103,'1999-01-24','1999-01-25');

