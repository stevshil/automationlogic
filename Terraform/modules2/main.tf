module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "${var.name}"
  cidr = "${var.cidr_block}"
  azs  = "${var.azs}"
  enable_dns_hostnames = true
  enable_dns_support   = true

  private_subnets = ["${priv_sub}"]
  public_subnets = ["${pub_sub}"]
  enable_nat_gateway = true
  single_nat_gateway = false
  reuse_nat_ips      = true
  one_nat_gateway_per_az = true
}
