data "template_file" "haproxy" {
  template = "${file("${path.module}/templates/haproxy.tpl")}"

  vars {
    #hosts = "${join(" ",aws_instance.web.*.public_ip)}"
    host1 = "server web ${element("${aws_instance.web.*.private_ip}",0)}:80"
    host2 = "server web ${element("${aws_instance.web.*.private_ip}",1)}:80"
    host3 = "server web ${element("${aws_instance.web.*.private_ip}",2)}:80"
    host4 = "server web ${element("${aws_instance.web.*.private_ip}",3)}:80"
  }
}

resource "aws_instance" "haproxy" {
  count           = 2
  ami             = "${lookup(var.image_id,var.region)}"
  instance_type   = "t2.micro"
  key_name        = "steveshilling"
  associate_public_ip_address = true
  security_groups = [ "${aws_security_group.allow_all.id}" ]
  subnet_id       = "${count.index == 1 ? "${module.public_subnets.az_subnet_ids["eu-west-3a"]}" : "${module.public_subnets.az_subnet_ids["eu-west-3b"]}"}"
  tags {
    Name = "stevesHAP"
  }
  user_data       = "${data.template_file.haproxy.rendered}"
}

output "public_ip" {
  value = "${aws_instance.haproxy.*.public_ip}"
}

output "template" {
  value = "${data.template_file.haproxy.rendered}"
}
