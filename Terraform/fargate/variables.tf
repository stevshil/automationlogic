variable "environment" {
}

variable "availability_zones" {
}

variable "private_subnets_cidr" {
}

variable "public_subnets_cidr" {
}

variable "allocated_storage" {
}

variable "database_name" {
}
variable "database_password" {
}
variable "database_username" {
}
variable "instance_class" {
}
variable "multi_az" {
}
variable "public_subnet_ids" {
}
variable "repository_name" {
}
variable "secret_key_base" {
}
variable "security_groups_ids}" {
}
variable "subnet_ids" {
}
variable "vpc_cidr" {
}
variable "vpc_id" {
}
