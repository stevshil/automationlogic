resource "aws_instance" "ec2" {
  ami                         = "${var.ami_id}"
  availability_zone           = "${var.az}"
  instance_type               = "${var.instance_type}"
  security_groups             = "${var.sec_grp}"
  associate_public_ip_address = true
  key_name                    = "${var.ssh_key}"

  tags {
    Name        = "${var.prefix}-EBSExample"
    Environment = "AL Academy"
  }
}

resource "aws_volume_attachment" "ebs_att_new" {
  count                       = "${var.create_ebs}"
  device_name                 = "/dev/xvdb"
  volume_id                   = "${aws_ebs_volume.xvdb.id}"
  instance_id                 = "${aws_instance.ec2.id}"
}

resource "aws_volume_attachment" "ebs_att_old" {
  count                       = "${var.use_existing_ebs}"
  device_name                 = "/dev/xvdb"
  volume_id                   = "${var.existing_ebs_id}"
  instance_id                 = "${aws_instance.ec2.id}"
}

resource "null_resource" "prepdisknew" {
  count         = "${var.create_ebs}"
  depends_on    = ["aws_volume_attachment.ebs_att_new"]
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = "${file("${var.ssh_file}")}"
    host        = "${aws_instance.ec2.public_ip}"
  }

  provisioner "file" {
    source      = "mkdisk.sh"
    destination = "/tmp/mkdisk.sh"
  }

  provisioner "remote-exec" {
    inline = [
    	"chmod +x /tmp/mkdisk.sh",
    	"sudo /tmp/mkdisk.sh"
    ]
  }
}

resource "null_resource" "prepdiskexisting" {
  count         = "${var.use_existing_ebs}"
  depends_on    = ["aws_volume_attachment.ebs_att_old"]
  connection {
    type        = "ssh"
    user        = "ec2-user"
    private_key = "${file("${var.ssh_file}")}"
    host        = "${aws_instance.ec2.public_ip}"
  }

  provisioner "file" {
    source      = "mkdisk.sh"
    destination = "/tmp/mkdisk.sh"
  }

  provisioner "remote-exec" {
    inline = [
    	"chmod +x /tmp/mkdisk.sh",
    	"sudo /tmp/mkdisk.sh"
    ]
  }
}

