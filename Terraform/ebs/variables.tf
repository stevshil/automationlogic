variable "secret_key" {}
variable "access_key" {}
variable "ssh_file" {}

variable "prefix" {
  default = "steve"
}

variable "create_ebs" {
  description = "Set to 1 to create a new EBS volume, 0 to use existing"
  default     = "0"
}

variable "use_existing_ebs" {
  description = "Set to 0 to use existing EBS Volume"
  default     = "1"
}

variable "existing_ebs_id" {
  description = "The ID of an existing EBS"
  default     = "vol-0afd88c25d1d74263"
}

variable "az" {
  default = "eu-west-2a"
}

variable "ami_id" {
  default = "ami-0274e11dced17bb5b"
}

variable "instance_type" {
  default = "t2.micro"
}

variable "ssh_key" {
  default = "steveshilling"
}

variable "sec_grp" {
  type    = "list"
  default = ["al-office"]
}

variable "disk_sizeGB" {
  default = "12"
}

variable "ebs_type" {
  default = "standard"
}
