variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "eu-west-2"
}

variable "image_id" {
  default = "ami-0274e11dced17bb5b"
}
