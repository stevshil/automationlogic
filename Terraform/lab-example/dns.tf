/*
  Add new DNS zone
*/
resource "aws_route53_zone" "primary" {
  name = "stevetf.grads.al-labs.co.uk"
}

/* Add this zone to the main one */
resource "aws_route53_record" "stevetf" {
  zone_id = "Z2U00Q95U7EKEA"
  name = "${var.dns_domain}"
  type = "NS"
  ttl = "30"

  records = [
    "${aws_route53_zone.primary.name_servers.0}",
    "${aws_route53_zone.primary.name_servers.1}",
    "${aws_route53_zone.primary.name_servers.2}",
    "${aws_route53_zone.primary.name_servers.3}",
  ]
}
