# terraformLab

This example is to show the grads how to build an infrastructure using Terraform.

![Terraform Lab](Docs/TerraformLab.png)

The above image is the infrastructure to build using Terraform, and each part should be contained in its own terraform code file, e.g.

* VPC, Public and Private subnets, NatGW should be in one file called *vpc.tf*
* Jenkins should be built from a backup Tar file in an S3 bucket, with a job that backs itself up nightly to the S3 bucket file, this file should be called *jenkins.tf*
* Private subnet should contain 2 ec2 instances running a PHP application that connects to the RDS as well as the ELB, this file should be called *webapp.tf*
* The RDS MySQL database should be created in a file called *db.tf* and should include the creation of the schema
* The bastion host should be contained in its own file called *bastion.tf*

## Example ideas
* You should start with just the vpc.tf, variables.tf, runme, aws.tf, terraform.tfvars, main.tf, outputs.tf
* Then add bastion.tf, but without the dns
* Then add the dns.tf
  - Note that this adds new sub domain to the existing
* Add the route53_record to the bastion
  - This updates only the DNS record
* Add in jenkins.tf
* Add in the DB, but with the VPC id missing from the security group
* DB will be created, but wrong VPC access
  - Note this will require a destruction of the DB to remove the SG
  - Use of the destroy with the -target option and the use of the object name
