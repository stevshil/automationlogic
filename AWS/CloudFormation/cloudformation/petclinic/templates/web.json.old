{
  "AWSTemplateFormatVersion": "2010-09-09",
  "Description": "PetClinic ASG ELB stack",
  "Parameters": {
    "InstanceType": {
      "Description": "Size of the instance",
      "Type": "String",
      "Default": "t2.micro"
    },
    "KeyName": {
      "Description": "SSH Key for instances",
      "Type": "String",
      "Default": "steveshilling"
    },
    "AMIID": {
      "Description": "AMI ID to launch",
      "Type": "String",
      "Default":  "ami-5652ce39"
    },
    "SSHLocation": {
      "Description": "Where you can log on from",
      "Type": "String",
      "Default": "77.108.144.180/32"
    },
    "Subnets": {
      "Description": "Subnets in VPC",
      "Type": "List<AWS::EC2::Subnet::Id>"
    }
  },
  "Resources": {
    "WebServers":  {
      "Type" : "AWS::AutoScaling::AutoScalingGroup",
      "Properties" : {
        "AvailabilityZones" : { "Fn::GetAZs" : ""},
        "LaunchConfigurationName" : { "Ref" : "LaunchConfig" },
        "MinSize" : "1",
        "MaxSize" : "3",
        "LoadBalancerNames" : [ { "Ref" : "ElasticLoadBalancer" } ]
      },
      "CreationPolicy" : {
        "ResourceSignal" : {
          "Timeout" : "PT15M",
          "Count"   : "1"
        }
      },
      "UpdatePolicy": {
        "AutoScalingRollingUpdate": {
          "MinInstancesInService": "1",
          "MaxBatchSize": "1",
          "PauseTime" : "PT15M",
          "WaitOnResourceSignals": "true"
        }
      }
    },
    "LaunchConfig" : {
      "Type" : "AWS::AutoScaling::LaunchConfiguration",
      "Metadata" : {
        "Comment" : "Install a simple application",
        "AWS::CloudFormation::Init" : {
          "files" : {
              "/etc/cfn/cfn-hup.conf" : {
                "content" : { "Fn::Join" : ["", [
                  "[main]\n",
                  "stack=", { "Ref" : "AWS::StackId" }, "\n",
                  "region=", { "Ref" : "AWS::Region" }, "\n"
                ]]},
                "mode"    : "000400",
                "owner"   : "root",
                "group"   : "root"
              },

              "/etc/cfn/hooks.d/cfn-auto-reloader.conf" : {
                "content": { "Fn::Join" : ["", [
                  "[cfn-auto-reloader-hook]\n",
                  "triggers=post.update\n",
                  "path=Resources.LaunchConfig.Metadata.AWS::CloudFormation::Init\n",
                  "action=/opt/aws/bin/cfn-init -v ",
                  "         --stack ", { "Ref" : "AWS::StackName" },
                  "         --resource LaunchConfig ",
                  "         --region ", { "Ref" : "AWS::Region" }, "\n",
                  "runas=root\n"
                ]]}
              }
            },
            "services" : {
              "sysvinit" : {
                "cfn-hup" : { "enabled" : "true", "ensureRunning" : "true",
                              "files" : ["/etc/cfn/cfn-hup.conf", "/etc/cfn/hooks.d/cfn-auto-reloader.conf"]}
              }
            }
          }
        }
      },
      "Properties" : {
        "KeyName" : { "Ref" : "KeyName" },
        "ImageId" : { "Ref": "AMIID" },
        "SecurityGroups" : [ { "Ref" : "InstanceSecurityGroup" } ],
        "InstanceType" : { "Ref" : "InstanceType" },
        "UserData"       : { "Fn::Base64" : { "Fn::Join" : ["", [
             "#!/bin/bash -xe\n",
             "yum update -y aws-cfn-bootstrap\n",

             "/opt/aws/bin/cfn-init -v ",
             "         --stack ", { "Ref" : "AWS::StackName" },
             "         --resource LaunchConfig ",
             "         --region ", { "Ref" : "AWS::Region" }, "\n",

             "/opt/aws/bin/cfn-signal -e $? ",
             "         --stack ", { "Ref" : "AWS::StackName" },
             "         --resource WebServerGroup ",
             "         --region ", { "Ref" : "AWS::Region" }, "\n"
        ]]}}
      },
      "WebServerScaleUpPolicy" : {
        "Type" : "AWS::AutoScaling::ScalingPolicy",
        "Properties" : {
          "AdjustmentType" : "ChangeInCapacity",
          "AutoScalingGroupName" : { "Ref" : "WebServerGroup" },
          "Cooldown" : "60",
          "ScalingAdjustment" : "1"
        }
      },
      "WebServerScaleDownPolicy" : {
        "Type" : "AWS::AutoScaling::ScalingPolicy",
        "Properties" : {
          "AdjustmentType" : "ChangeInCapacity",
          "AutoScalingGroupName" : { "Ref" : "WebServerGroup" },
          "Cooldown" : "60",
          "ScalingAdjustment" : "-1"
        }
      }
    },
    "ElasticLoadBalancer" : {
      "Type" : "AWS::ElasticLoadBalancing::LoadBalancer",
      "Properties" : {
        "AvailabilityZones" : { "Fn::GetAZs" : "" },
        "CrossZone" : "true",
        "Subnets": {"Ref": "Subnets"},
        "Listeners" : [ {
          "LoadBalancerPort" : "80",
          "InstancePort" : "8080",
          "Protocol" : "TCP"
        } ],
        "HealthCheck" : {
          "Target" : "TCP:8080",
          "HealthyThreshold" : "3",
          "UnhealthyThreshold" : "5",
          "Interval" : "30",
          "Timeout" : "5"
        }
      }
    },
    "InstanceSecurityGroup" : {
      "Type" : "AWS::EC2::SecurityGroup",
      "Properties" : {
        "GroupDescription" : "Enable SSH access and HTTP from the load balancer only",
        "SecurityGroupIngress" : [ {
          "IpProtocol" : "tcp",
          "FromPort" : "22",
          "ToPort" : "22",
          "CidrIp" : { "Ref" : "SSHLocation"}
        },
        {
          "IpProtocol" : "tcp",
          "FromPort" : "80",
          "ToPort" : "80",
          "SourceSecurityGroupOwnerId" : {"Fn::GetAtt" : ["ElasticLoadBalancer", "SourceSecurityGroup.OwnerAlias"]},
          "SourceSecurityGroupName" : {"Fn::GetAtt" : ["ElasticLoadBalancer", "SourceSecurityGroup.GroupName"]}
        },
        {
          "IpProtocol" : "tcp",
          "FromPort" : "8080",
          "ToPort" : "8080",
          "SourceSecurityGroupOwnerId" : {"Fn::GetAtt" : ["ElasticLoadBalancer", "SourceSecurityGroup.OwnerAlias"]},
          "SourceSecurityGroupName" : {"Fn::GetAtt" : ["ElasticLoadBalancer", "SourceSecurityGroup.GroupName"]}
        } ]
      }
  },
  "Outputs": {
    "ASGID": {
      "Description": "ID of the ASG",
      "Value": {"Ref": "WebServers"}
    },
    "LaunchID": {
      "Description": "ID of the Launch configuration",
      "Value": {"Ref": "LaunchConfig"}
    },
    "SGID": {
      "Description": "Security Group ID",
      "Value": {"Ref": "InstanceSecurityGroup"}
    },
    "ELBID": {
      "Description": "ID of the ELB",
      "Value": {"Ref": "ElasticLoadBalancer"}
    },
    "ELBDNS": {
      "Description": "End point of the ELB",
      "Value": {"Fn::GetAtt": [ "ElasticLoadBalancer", "DNSName"]}
    }
  }
}
