#!/usr/bin/python3

import boto3
import os
import re

credsloc=os.environ['HOME']+"/.aws/credentials"
creds=open(credsloc,"r")
creds.readline()
uid=re.sub(r"^.*= ","",creds.readline().rstrip())
key=re.sub(r"^.*= ","",creds.readline().rstrip())
creds.close()

tmplt=open("my.json","r")
tmpltdata=tmplt.read()
tmplt.close()

os.environ['AWS_DEFAULT_REGION']="us-east-1"
os.environ['AWS_ACCESS_KEY_ID']=uid
os.environ['AWS_SECRET_ACCESS_KEY']=key

client=boto3.client('cloudformation')

response=client.create_stack(
	StackName="SteveTest",
	TemplateBody=tmpltdata,
	TimeoutInMinutes=120,
	Capabilities=['CAPABILITY_IAM'],
	OnFailure="ROLLBACK",
	Tags=[{"Key": "NAME", "Value": "SteveStack"}]
)
