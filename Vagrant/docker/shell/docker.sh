#!/bin/bash

# Install docker software from community
yum -y install  yum-utils  device-mapper-persistent-data lvm2

# Set up docker software repository to allow install of docker software
yum-config-manager -y --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# Install Docker community edition
yum -y install docker-ce

# systemctl start docker
service docker start

# systemctl enable docker
chkconfig docker on

# Add vagrant user to docker group
usermod -G docker vagrant
