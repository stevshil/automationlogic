variable "vpcid" {
  type = "string"
  description = "VPC ID to place VM"
  default = "vpc-ed6ab384"
}

variable "aws_region" {
  description = "Region to host the image"
  default = "eu-central-1"
}

variable "aws_tag" {
  description = "Tag name to give to identify your instances, etc"
  default = "SteveTF"
}

variable "aws_key" {
  description = "SSH key for instances"
  default = "steveshilling"
}

variable "aws_instance_type" {
  description = "Instance type to launch vms with"
  default = "t2.micro"
}

variable "aws_image_name" {
  description = "Instance image id to use to start the vm"
  default = "dcos-centos7-*"
}

variable "aws_virtType" {
  description = "Virtualisation type"
  default = "hvm"
}

variable "aws_image_owner" {
  description = "Owner ID of the VM image"
  default = "273854932432"
}

variable "ssh_cidr" {
  type = "list"
  description = "Where is SSH allowed from"
  default = ["0.0.0.0/0"]
}

variable "web_cidr" {
  type = "list"
  description = "Where is SSH allowed from"
  default = ["0.0.0.0/0"]
}
