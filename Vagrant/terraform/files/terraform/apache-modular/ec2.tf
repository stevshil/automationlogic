# Create a new instance of the latest Ubuntu 14.04 on an
# t2.micro node with an AWS Tag naming it "HelloWorld"
provider "aws" {
  region = "${var.aws_region}"
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.aws_image_name}"]
  }

  filter {
    name   = "virtualization-type"
    values = ["${var.aws_virtType}"]
  }

  owners = ["${var.aws_image_owner}"]
}

resource "aws_security_group" "websg" {
  name = "vm_websg"
  description = "Allow ports 22 and 80"
  vpc_id = "${var.vpcid}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = "${var.ssh_cidr}"
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = "${var.web_cidr}"
  }

  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags {
    Name = "${var.aws_tag}SG"
  }
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.centos.id}"
  key_name = "${var.aws_key}"
  instance_type = "${var.aws_instance_type}"
  vpc_security_group_ids = ["${aws_security_group.websg.id}"]
  user_data = "${file("build.sh")}"

#  provisioner "remote-exec" {
#    inline = [
#      "sudo yum -y update",
#      "sudo yum -y install httpd",
#      "echo '<h1>Did the Earth move for you?</h1>' >/tmp/index.html",
#      "sudo mv /tmp/index.html /var/www/html",
#      "sudo chkconfig httpd on",
#      "sudo service httpd start"
#    ]
#
#    connection {
#      type = "ssh"
#      user = "ec2-user"
#      private_key = "${file("~/.ssh/id_rsa")}"
#    }
#  }

  tags {
    Name = "${var.aws_tag}Vm"
  }
}
