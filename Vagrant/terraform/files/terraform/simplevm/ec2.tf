# Create a new instance of the latest CentOS 7 on an
# t2.micro node with an AWS Tag naming it "SteveTestTF"
provider "aws" {
  region = "eu-central-1"
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["dcos-centos7-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["273854932432"]
}

resource "aws_instance" "web" {
  ami           = "${data.aws_ami.centos.id}"
  instance_type = "t2.micro"

  tags {
    Name = "SteveTestTF"
  }
}
