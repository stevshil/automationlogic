variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_key_name" {
	description = "AWS Keyname for SSH"
	default = "steveshilling"
}

variable "my_tag" {
	description = "Name to tag all elements with"
	default = "Steves"
}

variable "aws_region" {
    description = "EC2 Region for the VPC"
    default = "eu-central-1"
}

variable "amis" {
    description = "AMIs by region"
    default = {
        eu-west-1 = "ami-d834aba1"
	eu-west-2 = "ami-403e2524"
	us-east-1 = "ami-97785bed"
	eu-central-1 = "ami-5652ce39"
    }
}

variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.1.0/24"
}

variable "sshlocation" {
    description = "CIDR of location allowed SSH"
    default = "77.108.144.180/32"
}

variable "weblocation" {
    description = "CIDR of location allowed WEB access"
    default = "0.0.0.0/0"
}
